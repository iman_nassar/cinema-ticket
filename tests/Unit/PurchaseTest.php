<?php

namespace Tests\Unit;

use App\Ticket;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PurchaseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testPurchaseQuantityOutbound()
    {

        $this->withoutExceptionHandling();
        $availableTickets = 10;
        $requestedTickets = 20;
        $seller = factory(User::class)->create(['type'=>'seller']);
        $ticket = factory(Ticket::class)->create(['seller_id'=>$seller->id, 'quantity' => $availableTickets]);
        $buyer = factory(User::class)->create(['type'=>'buyer','balance'=>$ticket->price*$requestedTickets]);

        $response = $this->actingAs($buyer)
            ->post('/ticket/purchase/'.$ticket->id,['quantity' => $requestedTickets]);

        $this->assertDatabaseMissing('ticket_purchases', [
            'ticketId' => $ticket->id,
        ]);

    }

    public function testPurchase()
    {

        $this->withoutExceptionHandling();
        $availableTickets = 5;
        $requestedTickets = 4;
        $seller = factory(User::class)->create(['type'=>'seller']);
        $ticket = factory(Ticket::class)->create(['seller_id'=>$seller->id, 'quantity' => $availableTickets]);
        $buyer = factory(User::class)->create(['type'=>'buyer','balance'=>$ticket->price*$requestedTickets]);

        $this->actingAs($buyer)
            ->post('/ticket/purchase/'.$ticket->id,['quantity' => $requestedTickets]);

        $this->assertDatabaseHas('ticket_purchases', [
            'ticketId' => $ticket->id,
            'userId'=>$buyer->id,
            'quantity'=>$requestedTickets

        ]);
    }
}
