<?php

namespace App\Policies;

use App\Ticket;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;


class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any tickets.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the ticket.
     *
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function show(User $user, Ticket $ticket)
    {

    }

    /**
     * Determine whether the user can create tickets.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->type == 'seller';


    }

    /**
     * Determine whether the user can update the ticket.
     *
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function add(User $user)
    {
        return $user->type == 'seller';
    }

    public function update(User $user, Ticket $ticket)
    {
        return $ticket->seller_id == $user->id;

    }

    /**
     * Determine whether the user can delete the ticket.
     *
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function delete(User $user, Ticket $ticket)
    {
        //
        return $ticket->seller_id == $user->id
            ? Response::allow()
            : Response::deny('You Dont have Access to Delete This Ticket');
    }

    /**
     * Determine whether the user can restore the ticket.
     *
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function restore(User $user, Ticket $ticket)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ticket.
     *
     * @param \App\User $user
     * @param \App\Ticket $ticket
     * @return mixed
     */
    public function forceDelete(User $user, Ticket $ticket)
    {
        //
    }

    public function purchase(User $user)
    {
        //
        return $user->type == 'buyer'
            ? Response::allow()
            : Response::deny('You Dont have Access to Delete This Ticket');

    }
}
