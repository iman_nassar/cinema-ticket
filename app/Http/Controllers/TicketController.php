<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketPurchase;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TicketRequest;
use App\Http\Requests\PurchaseRequest;
use App\Mail\Mail;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->type == 'seller') {
            $tickets = Ticket::where([['seller_id', Auth::user()->id]])->get();
        } else {
            $tickets = Ticket::where([['status', 1], ['quantity', '>', 0]])->get();
        }
        return view('ticket.view', ["tickets" => $tickets]);
    }

    public function create()
    {
        $this->authorize('create', Ticket::class);
        return view('ticket.create');
    }


    public function store(TicketRequest $request)
    {
        $attributes = $request->validated();
        $attributes["status"] = $attributes["status"] ?? 0;
        $attributes["seller_id"] = Auth::user()->id;

        Ticket::create($attributes);
        return redirect('/ticket');
    }


    public function show($id)
    {

    }

    public function edit(Ticket $ticket)
    {

        $this->authorize('update', $ticket);
        //abort_if($ticket->getAttribute('seller_id')!=Auth::user()->id,403);
        return view('ticket.update', ["ticket" => $ticket]);
    }


    public function update(TicketRequest $request, Ticket $ticket)
    {

        $attributes = $request->validated();
        $attributes["status"] = $attributes["status"] ?? 0;
        $attributes["seller_id"] = Auth::user()->id;
        $ticket->update($attributes);
        return redirect('/ticket');

    }

    public function destroy(Ticket $ticket)
    {
        $this->authorize('delete', $ticket);
        $ticket->delete();
        return redirect('/ticket');
    }

    public function getPurchase($id)
    {

        $this->authorize('purchase', Ticket::class);
        $ticket = Ticket::findOrFail($id);
        return view('ticket.purchase', ['ticket' => $ticket]);
    }

    public function purchase(PurchaseRequest $request, $id)
    {

        $attributes = $request->validated();

        $attributes["ticketId"] = $id;
        $attributes["userId"]=Auth::user()->id;


        $user = User::find($attributes['userId']);
        $ticket = Ticket::findOrFail($id);
        if($ticket->quantity<$request->quantity)
            return response('Quantity out of bound',409);

        $totalPrice = $ticket->calculateTotalPrice($attributes['quantity']);

        if ($user['balance'] >= $totalPrice) {

            $ticketPurchase = new TicketPurchase();
            $ticketPurchase::create($attributes);

            $UserUpdateAttr = ['balance' => $user->calculateRemainingBalance($totalPrice)];
            $TicketUpdateAttr = ['quantity' => $ticket->calculateRemainingTickets($attributes['quantity'])];

            $ticket->update($TicketUpdateAttr);
            $user->update($UserUpdateAttr);

            $seller = User::find($ticket->seller_id)->first();
            $ticketPurchase->sendEmail($ticket['name'], $seller->email);


            return redirect('/ticket')->with(['success' => 'Successfully Purchased Ticket']);

        } else {
            return redirect('/ticket')->withErrors(['errorMessage' => 'Insufficient Fund']);
        }
    }


}
