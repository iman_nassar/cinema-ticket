<?php

namespace App;

use App\Mail\Mail;
use Illuminate\Database\Eloquent\Model;

class TicketPurchase extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function sendEmail($ticketName, $sellerEmail)
    {
        $details = [
            'title' => 'Cinema',
            'body' => 'A purchase has been done to Ticket:' . $ticketName
        ];
        \Mail::to($sellerEmail)->send(new Mail($details));

    }
}
