@extends('layouts.app')

@section('content')

    <div class="formDiv">

        <form method="POST" action="/ticket/{{$ticket->id}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter ticket name" required
                       value={{$ticket-> name}}>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" required>{{$ticket-> description}}</textarea>
            </div>
            <div class="form-group">
                <label>Quantity</label>
                <input name="quantity" type="number" required value={{$ticket-> quantity}} >
            </div>
            <div class="form-group">
                <label>Price</label>
                <input name="price" type="text" required value={{$ticket-> price}}>
                <label>$</label>

            </div>
            <div class="form-group">
                <label>Active</label>
                <input name="status" type="checkbox" value ="1" checked={{$ticket-> status}}>
            </div>


            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
