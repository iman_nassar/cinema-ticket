@extends('layouts.app')

@section('content')

    <div class="formDiv">

        <form method="POST" action="/ticket">
            {{ csrf_field() }}

            <div class="form-group">
                <label >Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter ticket name" >
            </div>
            <div class="form-group">
                <label >Description</label>
                <textarea class="form-control" name="description" required> </textarea>
            </div>
            <div class="form-group">
                <label >Quantity</label>
                <input name="quantity" type="number" required>
            </div>
            <div class="form-group">
                <label >Price</label>
                <input name="price" type="text" required>
                <label >$</label>

            </div>
            <div class="form-group">
                <label >Active</label>
                <input name="status" type="checkbox" value="1">
            </div>


            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="notification" is-danger>
            @foreach ($errors->all() as $error)
                <li>
                    {{$error}}
                </li>

            @endforeach
        </div>
    </div>
@endsection
