@extends('layouts.app')

@section('content')
    <script>
        $(document).ready(function () {
            $('.alert-success').fadeIn('slow').delay(2000).fadeOut();
            $('.alert-danger').fadeIn('slow').delay(2000).fadeOut();
        });
    </script>

    @if($errors->has('errorMessage'))
        <div class="alert alert-danger" role="alert">
            {{$errors->first('errorMessage')}}
        </div>
    @endif

    @if (\Session::has('success'))
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    @endif

    @if($tickets->isEmpty())
        <div class="alert alert-warning" role="alert">
            No Available Tickets at the moment
        </div>
    @endif

    @foreach ($tickets as $ticket)

        <div class="card" id="card">
            <div class="card-body">

                @if($ticket->status && Auth::user()->type == 'seller')
                    <h5 class="card-title">
                        {{$ticket->name}} : Active
                    </h5>

                @elseif(!$ticket->status && Auth::user()->type == 'seller')
                    <h5 class="card-title">
                        {{$ticket->name}} : InActive
                    </h5>
                @elseif(Auth::user()->type == 'buyer')
                    <h5 class="card-title">
                        {{$ticket->name}}
                    </h5>

                @endif

                <p class="card-text">
                    {{$ticket->description}}
                </p>

                <p>
                    Tickets left: {{$ticket->quantity}}
                </p>
                <b>
                    {{$ticket->price}} $
                </b>
                @if(Auth::user()->type == 'seller')
                    <a href="{{ url('/ticket/' . $ticket->id . '/edit') }}"
                       class="btn btn-xs btn-info pull-right">Edit</a>
                    <form method="POST" action="/ticket/{{$ticket->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button id="deleteBtn" type="submit">
                            <i class="material-icons">
                                delete
                            </i>
                        </button>
                    </form>
                @elseif(Auth::user()->type == 'buyer')
                    <a href="{{ url('/ticket/purchase/'. $ticket->id) }}" class="btn btn-xs btn-info pull-right">Purchase</a>
                @endif

            </div>
        </div>
        @endforeach
        </ul>

@endsection
