@extends('layouts.app')

@section('content')

    <div class="formDiv">
        <form id='purchaseForm' method="POST" action="/ticket/purchase/{{$ticket->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Name : {{$ticket->name}}</label>
            </div>
            <div class="form-group">
                <label>Description : {{$ticket->description}}</label>
            </div>
            <div class="form-group">
                <label>Price : </label>
                <label>{{$ticket->price}}</label>
                <label>$</label>
            </div>
            <div class="form-group">
                <label>Quantity</label>
                <input id="quantity" name="quantity" type="number" required onchange="calculateTotal()" value="1"
                       min="1">
            </div>
            <p id="total">
                Total Amount = {{$ticket->price}}
            </p>
            <script type="application/javascript">
                let price = '{{ $ticket->price}}';

                function calculateTotal() {
                    var quantity = parseInt(document.getElementById('quantity').value, 10);
                    document.getElementById('total').innerHTML = "Total Amount = " + price * quantity;
                }

            </script>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
