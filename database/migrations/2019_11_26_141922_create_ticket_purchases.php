<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('ticketId');
            $table->unsignedInteger('userId');
            $table->unsignedInteger('quantity');

            $table->foreign('userId')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('ticketId')
                ->references('id')->on('tickets')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_purchases');
    }
}
