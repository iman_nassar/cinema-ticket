<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => Str::random(10),
        'status' => $faker->randomElement([0,1]),
        'quantity' => $faker->numberBetween(0,5),
        'price' => $faker->numberBetween(1,20),
        'seller_id' => $faker->randomDigit];
});
